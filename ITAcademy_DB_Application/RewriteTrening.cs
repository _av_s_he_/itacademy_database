﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ITAcademy_DB_Application
{
    public partial class RewriteTrening : Form
    {
        const string _sqlConnectionString = "Server=tcp:itacademy.database.windows.net,1433;" +
            "Database=Ravsher;" +
            "User ID=Ravsher;Password=Sduq97223;" +
            "Trusted_Connection=False;Encrypt=True;";
        public RewriteTrening()
        {
            InitializeComponent();
        }
        private void btnMainMenu_Click(object sender, EventArgs e)
        {
            ActiveForm.Hide();
            MainMenu Menu = new MainMenu();
            Menu.ShowDialog();
            Close();
        }
        private void RewriteTrening_Load(object sender, EventArgs e)
        {
            GetTreningList();
            GetTrenerList();
            GetTestLink();
        }
        private void GetTreningList()
        {

            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            SqlCommand comm = new SqlCommand($"SELECT * FROM [dbo].[Trening]", con);
            SqlDataReader DR = comm.ExecuteReader();

            cmbTrening.Items.Clear();
            while (DR.Read())
            {
                Trening trng = new Trening(int.Parse(DR[0].ToString()), DR[1].ToString(), DR[2].ToString(), DR[3].ToString(), int.Parse(DR[4].ToString()));
                cmbTrening.Items.Add(trng);
                cmbTrening.DisplayMember = "name";
                cmbTreningQuestion.Items.Add(trng);
                cmbTreningQuestion.DisplayMember = "name";
            }

            con.Close();
        }
        private void GetTrenerList()
        {
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            SqlCommand comm = new SqlCommand($"SELECT * FROM [dbo].[Trener]", con);
            SqlDataReader DR = comm.ExecuteReader();

            cmbTrener.Items.Clear();
            while (DR.Read())
            {
                Trener trnr = new Trener(int.Parse(DR[0].ToString()), DR[1].ToString(), DR[2].ToString());
                cmbTrener.Items.Add(trnr);
                cmbTrener.DisplayMember = "name";
                cmbTrenerID.Items.Add(trnr);
                cmbTrenerID.DisplayMember = "name";
            }
            con.Close();
        }

        private void cmbTrening_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeTreningRewriteInfo();
        }
        private void ChangeTreningRewriteInfo()
        {
            Trening trng = (Trening)cmbTrening.SelectedItem;
            txtTreningName.Text = trng.name;
            txtStartTrening.Text = trng.startTime;
            txtEndTrening.Text = trng.endTime;
            GetTreningTrenerID();
        }

        private void GetTreningTrenerID()
        {
            Trening trng = (Trening)cmbTrening.SelectedItem;
            for (int i = 0; i < cmbTrenerID.Items.Count; i++)
            {
                Trener trnr = (Trener)cmbTrenerID.Items[i];
                if (trnr.id == trng.trenerId)
                {
                    cmbTrenerID.SelectedItem = cmbTrenerID.Items[i];
                    break;
                }
            }
        }

        private void btnRewriteTrening_Click(object sender, EventArgs e)
        {
            Trening trng = (Trening)cmbTrening.SelectedItem;
            Trener trnr = (Trener)cmbTrenerID.SelectedItem;
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            try
            {
                if ((txtTreningName.Text == "" || txtTreningName.Text.Length > 50) ||
                    (txtStartTrening.Text == "" || txtEndTrening.Text == ""))
                {
                    throw new Exception();
                }
                SqlCommand comm = new SqlCommand($"UPDATE [dbo].[Trening] SET Trening_Name = '{txtTreningName.Text}', Start_Data = '{txtStartTrening.Text}', End_Data = '{txtEndTrening.Text}', TrenerID = {trnr.id} WHERE ID = {trng.id}", con);
                comm.ExecuteNonQuery();
                txtTreningName.Text = null;
                txtEndTrening.Text = null;
                txtStartTrening.Text = null;
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Це ж як треба було постаратись що б хапнути NullReferenceException в полі які заповняються автоматично? =)");
            }
            catch (Exception)
            {
                MessageBox.Show("Не коректно введені дані");
            }
            con.Close();
        }

        private void cmbTrener_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetTrenerInfo();
        }
        private void GetTrenerInfo()
        {
            Trener trnr = (Trener)cmbTrener.SelectedItem;
            txtTrenerName.Text = trnr.name;
            txtTrenerBD.Text = trnr.birthDay;
        }

        private void btnRewriteTrener_Click(object sender, EventArgs e)
        {
            Trener trnr = (Trener)cmbTrener.SelectedItem;
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            try
            {
                if ((txtTrenerName.Text == "" || txtTrenerName.Text.Length > 50) || (txtTrenerBD.Text == ""))
                {
                    throw new Exception();
                }
                SqlCommand comm = new SqlCommand($"UPDATE [dbo].[Trener] SET PIB = '{txtTrenerName.Text}', BD_Date = '{txtTrenerBD.Text}' WHERE ID = {trnr.id}", con);
                comm.ExecuteNonQuery();
                txtTrenerBD.Text = null;
                txtTrenerName.Text = null;
            }
            catch (Exception)
            {
                MessageBox.Show("Не коректно введені дані");
            }

            con.Close();
        }

        private void cmbTreningQuestion_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetModuleQuestion();
            cmbQuestion.Items.Clear();
            cmbQuestion.Text = "Виберіть питання для редагування";
            cmbTestQuestion.Items.Clear();
            cmbTestQuestion.Text = "Виберіть тест для редагування питання";
            cmbModuleQuestion.Text = "Виберіть модуль для редагування питання";
            ClearQuestionBoxes();
        }
        private void GetModuleQuestion()
        {
            Trening trng = (Trening)cmbTreningQuestion.SelectedItem;
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            SqlCommand comm = new SqlCommand($"SELECT * FROM [dbo].[Module] WHERE TreningID = {trng.id}", con);
            SqlDataReader DR = comm.ExecuteReader();

            cmbModuleQuestion.Items.Clear();
            while (DR.Read())
            {
                Module mdl = new Module(int.Parse(DR[0].ToString()), DR[1].ToString(), DR[2].ToString(), DR[3].ToString(), int.Parse(DR[4].ToString()));
                cmbModuleQuestion.Items.Add(mdl);
                cmbModuleQuestion.DisplayMember = "name";
            }
            con.Close();
        }

        private void cmbModuleQuestion_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetTestQuestion();
            cmbQuestion.Items.Clear();
            cmbQuestion.Text = "Виберіть питання для редагування";
            cmbTestQuestion.Text = "Виберіть тест для редагування питання";
            ClearQuestionBoxes();
        }

        private void GetTestQuestion()
        {
            Module mdl = (Module)cmbModuleQuestion.SelectedItem;
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            SqlCommand comm = new SqlCommand($"SELECT * FROM [dbo].[Test] WHERE ModuleID = {mdl.id}", con);
            SqlDataReader DR = comm.ExecuteReader();

            cmbTestQuestion.Items.Clear();
            while (DR.Read())
            {
                Test tst = new Test(int.Parse(DR[0].ToString()), DR[1].ToString(), int.Parse(DR[2].ToString()));
                cmbTestQuestion.Items.Add(tst);
                cmbTestQuestion.DisplayMember = "name";
            }
            con.Close();
        }

        private void cmbTestQuestion_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetQuestionID();
            cmbQuestion.Text = "Виберіть питання для редагування";
        }
        private void GetQuestionID()
        {
            Test tst = (Test)cmbTestQuestion.SelectedItem;
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            SqlCommand comm = new SqlCommand($"SELECT * FROM [dbo].[Question] WHERE TestID = {tst.id}", con);
            SqlDataReader DR = comm.ExecuteReader();

            cmbQuestion.Items.Clear();
            while (DR.Read())
            {
                Question qstn = new Question(int.Parse(DR[0].ToString()), DR[1].ToString(), DR[2].ToString(), DR[3].ToString(), DR[4].ToString(), DR[5].ToString(), int.Parse(DR[6].ToString()));
                cmbQuestion.Items.Add(qstn);
                cmbQuestion.DisplayMember = "question";
            }
            con.Close();
        }

        private void cmbQuestion_SelectedIndexChanged(object sender, EventArgs e)
        {
            RewriteQuestion();
        }
        private void RewriteQuestion()
        {
            Question qstn = (Question)cmbQuestion.SelectedItem;
            txtQuestion.Text = qstn.question;
            txtAnswer1.Text = qstn.answer1;
            txtAnswer2.Text = qstn.answer2;
            txtAnswer3.Text = qstn.answer3;
            txtRightAnswer.Text = qstn.rightAnswer;
            GetQuestionTestID(qstn);
        }
        private void GetQuestionTestID(Question qstn)
        {
            for (int i = 0; i < cmbQuestionTestID.Items.Count; i++)
            {
                Test tst = (Test)cmbQuestionTestID.Items[i];
                if (tst.id == qstn.testId)
                {
                    cmbQuestionTestID.SelectedItem = cmbQuestionTestID.Items[i];
                    break;
                }
            }
        }
        private void GetTestLink()
        {
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            SqlCommand comm = new SqlCommand($"SELECT * FROM [dbo].[Test]", con);
            SqlDataReader DR = comm.ExecuteReader();

            cmbQuestionTestID.Items.Clear();
            while (DR.Read())
            {
                Test tst = new Test(int.Parse(DR[0].ToString()), DR[1].ToString(), int.Parse(DR[2].ToString()));
                cmbQuestionTestID.Items.Add(tst);
                cmbQuestionTestID.DisplayMember = "name";
            }
            con.Close();
        }
        private void ClearQuestionBoxes()
        {
            txtQuestion.Text = null;
            txtAnswer1.Text = null;
            txtAnswer2.Text = null;
            txtAnswer3.Text = null;
            txtRightAnswer.Text = null;
            cmbQuestionTestID.Text = "Виберіть питання для початку";
        }

        private void btnRewriteQuestion_Click(object sender, EventArgs e)
        {
            Test tst = (Test)cmbQuestionTestID.SelectedItem;
            Question qstn = (Question)cmbQuestion.SelectedItem;
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            try
            {
                if ((txtQuestion.Text == "" || txtQuestion.Text.Length > 300) || (txtAnswer1.Text == "" || txtAnswer1.Text.Length > 200) || 
                    (txtAnswer2.Text == "" || txtAnswer2.Text.Length > 200) || (txtAnswer3.Text == "" || txtAnswer3.Text.Length > 200) ||
                    (txtRightAnswer.Text == "" || txtRightAnswer.Text.Length > 200) || cmbQuestionTestID.SelectedItem == null)
                {
                    throw new Exception();
                }
                SqlCommand comm = new SqlCommand($"UPDATE [dbo].[Question] SET Question = '{txtQuestion.Text}', Answer1 = '{txtAnswer1.Text}', Answer2 = '{txtAnswer2.Text}', Answer3 = '{txtAnswer3.Text}', Right_Answer = '{txtRightAnswer.Text}', TestID = {tst.id} WHERE ID = {qstn.id}", con);
                comm.ExecuteNonQuery();
                ClearQuestionBoxes();

                cmbQuestion.Items.Clear();
                cmbQuestion.Text = "Виберіть питання для редагування";
                cmbTestQuestion.Items.Clear();
                cmbTestQuestion.Text = "Виберіть тест для редагування питання";
                cmbModuleQuestion.Items.Clear();
                cmbModuleQuestion.Text = "Виберіть модуль для редагування питання";
                cmbTreningQuestion.Items.Clear();
                cmbTreningQuestion.Text = "Виберіть тренінг для редагування питання";
            }
            catch (Exception)
            {
                MessageBox.Show("Не коректно введені дані");
            }

            con.Close();
        }
    }
}
