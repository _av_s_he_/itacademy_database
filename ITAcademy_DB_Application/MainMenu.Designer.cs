﻿namespace ITAcademy_DB_Application
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnZvit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.Desktop;
            this.button1.Location = new System.Drawing.Point(304, 305);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(170, 88);
            this.button1.TabIndex = 0;
            this.button1.Text = "Переглянути таблиці";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.Desktop;
            this.button2.Location = new System.Drawing.Point(545, 305);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(170, 88);
            this.button2.TabIndex = 1;
            this.button2.Text = "Добавити поля в таблиці";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnZvit
            // 
            this.btnZvit.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnZvit.Location = new System.Drawing.Point(769, 305);
            this.btnZvit.Name = "btnZvit";
            this.btnZvit.Size = new System.Drawing.Size(170, 88);
            this.btnZvit.TabIndex = 2;
            this.btnZvit.Text = "Переглянути звіти до лабораторної";
            this.btnZvit.UseVisualStyleBackColor = false;
            this.btnZvit.Click += new System.EventHandler(this.btnZvit_Click);
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Desktop;
            this.ClientSize = new System.Drawing.Size(1292, 709);
            this.Controls.Add(this.btnZvit);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.ForeColor = System.Drawing.Color.Lime;
            this.Name = "MainMenu";
            this.Text = "MainMenu";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnZvit;
    }
}