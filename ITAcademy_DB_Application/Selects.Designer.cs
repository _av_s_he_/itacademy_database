﻿namespace ITAcademy_DB_Application
{
    partial class Selects
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbUsersList = new System.Windows.Forms.ComboBox();
            this.btnGetUserModuleInfo = new System.Windows.Forms.Button();
            this.dgvUserInfo = new System.Windows.Forms.DataGridView();
            this.btnGetUserTestInfo = new System.Windows.Forms.Button();
            this.btnTableFilling = new System.Windows.Forms.Button();
            this.btnMainMenu = new System.Windows.Forms.Button();
            this.btnGetTreningInfo = new System.Windows.Forms.Button();
            this.btnGetModuleLinksInfo = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUserInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbUsersList
            // 
            this.cmbUsersList.BackColor = System.Drawing.SystemColors.Desktop;
            this.cmbUsersList.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmbUsersList.ForeColor = System.Drawing.Color.Lime;
            this.cmbUsersList.FormattingEnabled = true;
            this.cmbUsersList.Location = new System.Drawing.Point(29, 30);
            this.cmbUsersList.Name = "cmbUsersList";
            this.cmbUsersList.Size = new System.Drawing.Size(291, 23);
            this.cmbUsersList.TabIndex = 0;
            this.cmbUsersList.Text = "UsersList";
            this.cmbUsersList.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // btnGetUserModuleInfo
            // 
            this.btnGetUserModuleInfo.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnGetUserModuleInfo.ForeColor = System.Drawing.Color.Lime;
            this.btnGetUserModuleInfo.Location = new System.Drawing.Point(878, 30);
            this.btnGetUserModuleInfo.Name = "btnGetUserModuleInfo";
            this.btnGetUserModuleInfo.Size = new System.Drawing.Size(190, 54);
            this.btnGetUserModuleInfo.TabIndex = 1;
            this.btnGetUserModuleInfo.Text = "Переглянути пройдені модулі користувача";
            this.btnGetUserModuleInfo.UseVisualStyleBackColor = false;
            this.btnGetUserModuleInfo.Click += new System.EventHandler(this.button1_Click);
            // 
            // dgvUserInfo
            // 
            this.dgvUserInfo.BackgroundColor = System.Drawing.SystemColors.Desktop;
            this.dgvUserInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUserInfo.GridColor = System.Drawing.SystemColors.Desktop;
            this.dgvUserInfo.Location = new System.Drawing.Point(29, 100);
            this.dgvUserInfo.Name = "dgvUserInfo";
            this.dgvUserInfo.Size = new System.Drawing.Size(1240, 520);
            this.dgvUserInfo.TabIndex = 2;
            this.dgvUserInfo.Text = "dataGridView1";
            // 
            // btnGetUserTestInfo
            // 
            this.btnGetUserTestInfo.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnGetUserTestInfo.ForeColor = System.Drawing.Color.Lime;
            this.btnGetUserTestInfo.Location = new System.Drawing.Point(1074, 30);
            this.btnGetUserTestInfo.Name = "btnGetUserTestInfo";
            this.btnGetUserTestInfo.Size = new System.Drawing.Size(195, 54);
            this.btnGetUserTestInfo.TabIndex = 3;
            this.btnGetUserTestInfo.Text = "Переглянути відповіді користувача на тести";
            this.btnGetUserTestInfo.UseVisualStyleBackColor = false;
            this.btnGetUserTestInfo.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // btnTableFilling
            // 
            this.btnTableFilling.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnTableFilling.ForeColor = System.Drawing.Color.Lime;
            this.btnTableFilling.Location = new System.Drawing.Point(1074, 626);
            this.btnTableFilling.Name = "btnTableFilling";
            this.btnTableFilling.Size = new System.Drawing.Size(195, 54);
            this.btnTableFilling.TabIndex = 4;
            this.btnTableFilling.Text = "Заповнити поля таблиць -->";
            this.btnTableFilling.UseVisualStyleBackColor = false;
            this.btnTableFilling.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // btnMainMenu
            // 
            this.btnMainMenu.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnMainMenu.ForeColor = System.Drawing.Color.Lime;
            this.btnMainMenu.Location = new System.Drawing.Point(29, 626);
            this.btnMainMenu.Name = "btnMainMenu";
            this.btnMainMenu.Size = new System.Drawing.Size(195, 53);
            this.btnMainMenu.TabIndex = 5;
            this.btnMainMenu.Text = "<-- Назад в головне меню";
            this.btnMainMenu.UseVisualStyleBackColor = false;
            this.btnMainMenu.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnGetTreningInfo
            // 
            this.btnGetTreningInfo.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnGetTreningInfo.ForeColor = System.Drawing.Color.Lime;
            this.btnGetTreningInfo.Location = new System.Drawing.Point(682, 30);
            this.btnGetTreningInfo.Name = "btnGetTreningInfo";
            this.btnGetTreningInfo.Size = new System.Drawing.Size(190, 54);
            this.btnGetTreningInfo.TabIndex = 6;
            this.btnGetTreningInfo.Text = "Переглянути тренінги на які записаний користувач";
            this.btnGetTreningInfo.UseVisualStyleBackColor = false;
            this.btnGetTreningInfo.Click += new System.EventHandler(this.btnGetTreningInfo_Click);
            // 
            // btnGetModuleLinksInfo
            // 
            this.btnGetModuleLinksInfo.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnGetModuleLinksInfo.ForeColor = System.Drawing.Color.Lime;
            this.btnGetModuleLinksInfo.Location = new System.Drawing.Point(486, 30);
            this.btnGetModuleLinksInfo.Name = "btnGetModuleLinksInfo";
            this.btnGetModuleLinksInfo.Size = new System.Drawing.Size(190, 54);
            this.btnGetModuleLinksInfo.TabIndex = 7;
            this.btnGetModuleLinksInfo.Text = "Переглянути посилання на матеріали для модулів користувача";
            this.btnGetModuleLinksInfo.UseVisualStyleBackColor = false;
            this.btnGetModuleLinksInfo.Click += new System.EventHandler(this.btnGetModuleLinksInfo_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Desktop;
            this.ClientSize = new System.Drawing.Size(1288, 710);
            this.Controls.Add(this.btnGetModuleLinksInfo);
            this.Controls.Add(this.btnGetTreningInfo);
            this.Controls.Add(this.btnMainMenu);
            this.Controls.Add(this.btnTableFilling);
            this.Controls.Add(this.btnGetUserTestInfo);
            this.Controls.Add(this.dgvUserInfo);
            this.Controls.Add(this.btnGetUserModuleInfo);
            this.Controls.Add(this.cmbUsersList);
            this.ForeColor = System.Drawing.SystemColors.Desktop;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.dgvUserInfo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbUsersList;
        private System.Windows.Forms.Button btnGetUserModuleInfo;
        private System.Windows.Forms.DataGridView dgvUserInfo;
        private System.Windows.Forms.Button btnGetUserTestInfo;
        private System.Windows.Forms.Button btnTableFilling;
        private System.Windows.Forms.Button btnMainMenu;
        private System.Windows.Forms.Button btnGetTreningInfo;
        private System.Windows.Forms.Button btnGetModuleLinksInfo;
    }
}

