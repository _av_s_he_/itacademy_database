﻿namespace ITAcademy_DB_Application
{
    partial class Lab
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnMainMenu = new System.Windows.Forms.Button();
            this.btnSelects = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbTrener = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtN = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbUserTrener = new System.Windows.Forms.ComboBox();
            this.cmbUser = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dgvAnswer = new System.Windows.Forms.DataGridView();
            this.btnSelect1 = new System.Windows.Forms.Button();
            this.btnSelect2 = new System.Windows.Forms.Button();
            this.btnSelect3 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAnswer)).BeginInit();
            this.SuspendLayout();
            // 
            // btnMainMenu
            // 
            this.btnMainMenu.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnMainMenu.ForeColor = System.Drawing.Color.Lime;
            this.btnMainMenu.Location = new System.Drawing.Point(13, 13);
            this.btnMainMenu.Name = "btnMainMenu";
            this.btnMainMenu.Size = new System.Drawing.Size(151, 65);
            this.btnMainMenu.TabIndex = 0;
            this.btnMainMenu.Text = "<-- Назад на головне меню";
            this.btnMainMenu.UseVisualStyleBackColor = false;
            this.btnMainMenu.Click += new System.EventHandler(this.btnMainMenu_Click);
            // 
            // btnSelects
            // 
            this.btnSelects.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnSelects.ForeColor = System.Drawing.Color.Lime;
            this.btnSelects.Location = new System.Drawing.Point(1131, 13);
            this.btnSelects.Name = "btnSelects";
            this.btnSelects.Size = new System.Drawing.Size(151, 65);
            this.btnSelects.TabIndex = 1;
            this.btnSelects.Text = "Інші селекти -->";
            this.btnSelects.UseVisualStyleBackColor = false;
            this.btnSelects.Click += new System.EventHandler(this.btnSelects_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.Color.Lime;
            this.label1.Location = new System.Drawing.Point(13, 108);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(661, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "1.Вивести всі модулі (вказавши назву модуля, тренінг до якого належить модуль і д" +
    "ату початку) які читає тренер:";
            // 
            // cmbTrener
            // 
            this.cmbTrener.BackColor = System.Drawing.SystemColors.Desktop;
            this.cmbTrener.ForeColor = System.Drawing.Color.Lime;
            this.cmbTrener.FormattingEnabled = true;
            this.cmbTrener.Location = new System.Drawing.Point(680, 105);
            this.cmbTrener.Name = "cmbTrener";
            this.cmbTrener.Size = new System.Drawing.Size(179, 23);
            this.cmbTrener.TabIndex = 3;
            this.cmbTrener.Text = "Оберіть тренера";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.ForeColor = System.Drawing.Color.Lime;
            this.label2.Location = new System.Drawing.Point(13, 140);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(633, 30);
            this.label2.TabIndex = 4;
            this.label2.Text = "2.Вивести список користувачів (ПІБ, дата народження) і назв модулів, тести до яки" +
    "х користувачі пройшли з\r\n\r\n";
            // 
            // txtN
            // 
            this.txtN.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtN.ForeColor = System.Drawing.Color.Lime;
            this.txtN.Location = new System.Drawing.Point(652, 137);
            this.txtN.Name = "txtN";
            this.txtN.Size = new System.Drawing.Size(30, 23);
            this.txtN.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label3.ForeColor = System.Drawing.Color.Lime;
            this.label3.Location = new System.Drawing.Point(688, 140);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "разу";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label4.ForeColor = System.Drawing.Color.Lime;
            this.label4.Location = new System.Drawing.Point(13, 174);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(354, 15);
            this.label4.TabIndex = 7;
            this.label4.Text = "3.Вивести список правильних всіх відповідей користувача \r\n";
            // 
            // cmbUserTrener
            // 
            this.cmbUserTrener.BackColor = System.Drawing.SystemColors.Desktop;
            this.cmbUserTrener.ForeColor = System.Drawing.Color.Lime;
            this.cmbUserTrener.FormattingEnabled = true;
            this.cmbUserTrener.Location = new System.Drawing.Point(762, 171);
            this.cmbUserTrener.Name = "cmbUserTrener";
            this.cmbUserTrener.Size = new System.Drawing.Size(179, 23);
            this.cmbUserTrener.TabIndex = 8;
            this.cmbUserTrener.Text = "Оберіть тренера";
            // 
            // cmbUser
            // 
            this.cmbUser.BackColor = System.Drawing.SystemColors.Desktop;
            this.cmbUser.ForeColor = System.Drawing.Color.Lime;
            this.cmbUser.FormattingEnabled = true;
            this.cmbUser.Location = new System.Drawing.Point(373, 171);
            this.cmbUser.Name = "cmbUser";
            this.cmbUser.Size = new System.Drawing.Size(179, 23);
            this.cmbUser.TabIndex = 9;
            this.cmbUser.Text = "Оберіть користувача";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label5.ForeColor = System.Drawing.Color.Lime;
            this.label5.Location = new System.Drawing.Point(558, 174);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(198, 15);
            this.label5.TabIndex = 10;
            this.label5.Text = "на всіх модулях, які веде тренер ";
            // 
            // dgvAnswer
            // 
            this.dgvAnswer.BackgroundColor = System.Drawing.SystemColors.Desktop;
            this.dgvAnswer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAnswer.GridColor = System.Drawing.Color.Lime;
            this.dgvAnswer.Location = new System.Drawing.Point(13, 223);
            this.dgvAnswer.Name = "dgvAnswer";
            this.dgvAnswer.Size = new System.Drawing.Size(1269, 475);
            this.dgvAnswer.TabIndex = 11;
            this.dgvAnswer.TabStop = false;
            this.dgvAnswer.Text = "dataGridView1";
            // 
            // btnSelect1
            // 
            this.btnSelect1.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnSelect1.ForeColor = System.Drawing.Color.Lime;
            this.btnSelect1.Location = new System.Drawing.Point(1131, 104);
            this.btnSelect1.Name = "btnSelect1";
            this.btnSelect1.Size = new System.Drawing.Size(151, 23);
            this.btnSelect1.TabIndex = 12;
            this.btnSelect1.Text = "Виконати 1 запит";
            this.btnSelect1.UseVisualStyleBackColor = false;
            this.btnSelect1.Click += new System.EventHandler(this.btnSelect1_Click);
            // 
            // btnSelect2
            // 
            this.btnSelect2.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnSelect2.ForeColor = System.Drawing.Color.Lime;
            this.btnSelect2.Location = new System.Drawing.Point(1131, 136);
            this.btnSelect2.Name = "btnSelect2";
            this.btnSelect2.Size = new System.Drawing.Size(151, 23);
            this.btnSelect2.TabIndex = 13;
            this.btnSelect2.Text = "Виконати 2 запит";
            this.btnSelect2.UseVisualStyleBackColor = false;
            this.btnSelect2.Click += new System.EventHandler(this.btnSelect2_Click);
            // 
            // btnSelect3
            // 
            this.btnSelect3.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnSelect3.ForeColor = System.Drawing.Color.Lime;
            this.btnSelect3.Location = new System.Drawing.Point(1131, 170);
            this.btnSelect3.Name = "btnSelect3";
            this.btnSelect3.Size = new System.Drawing.Size(151, 23);
            this.btnSelect3.TabIndex = 14;
            this.btnSelect3.Text = "Виконати 3 запит";
            this.btnSelect3.UseVisualStyleBackColor = false;
            this.btnSelect3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Lab
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Desktop;
            this.ClientSize = new System.Drawing.Size(1294, 710);
            this.Controls.Add(this.btnSelect3);
            this.Controls.Add(this.btnSelect2);
            this.Controls.Add(this.btnSelect1);
            this.Controls.Add(this.dgvAnswer);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cmbUser);
            this.Controls.Add(this.cmbUserTrener);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtN);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbTrener);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSelects);
            this.Controls.Add(this.btnMainMenu);
            this.Name = "Lab";
            this.Text = "Lab";
            this.Load += new System.EventHandler(this.Lab_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAnswer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnMainMenu;
        private System.Windows.Forms.Button btnSelects;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbTrener;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtN;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbUserTrener;
        private System.Windows.Forms.ComboBox cmbUser;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dgvAnswer;
        private System.Windows.Forms.Button btnSelect1;
        private System.Windows.Forms.Button btnSelect2;
        private System.Windows.Forms.Button btnSelect3;
    }
}