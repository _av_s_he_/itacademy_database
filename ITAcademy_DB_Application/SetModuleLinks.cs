﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ITAcademy_DB_Application
{
    public partial class SetModuleLinks : Form
    {
        const string _sqlConnectionString = "Server=tcp:itacademy.database.windows.net,1433;" +
            "Database=Ravsher;" +
            "User ID=Ravsher;Password=Sduq97223;" +
            "Trusted_Connection=False;Encrypt=True;";
        public SetModuleLinks()
        {
            InitializeComponent();
        }

        private void SetModuleLinks_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            SqlCommand comm = new SqlCommand("select * from [dbo].[Module]", con);
            SqlDataReader DR = comm.ExecuteReader();

            cmbModule.Items.Clear();
            while (DR.Read())
            {
                Module mdl = new Module(int.Parse(DR[0].ToString()), DR[1].ToString(), DR[2].ToString(), DR[3].ToString(), int.Parse(DR[4].ToString()));
                cmbModule.Items.Add(mdl);
                cmbModule.DisplayMember = "name";
            }

            con.Close();
        }

        private void btnSetModuleLink_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();
            try
            {
                if (txtModuleLink.Text == "" || txtModuleLink.Text.Length > 50)
                {
                    throw new Exception();
                }
                Module mdl = (Module)cmbModule.SelectedItem;
                SqlCommand comm = new SqlCommand($"INSERT INTO[dbo].[Link](Link, ModuleID) VALUES('{txtModuleLink.Text}',{mdl.id})", con);
                comm.ExecuteNonQuery();

                GetInfoFromDataBase();
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Виберіть модуль для заповнення");
            }
            catch (Exception)
            {
                MessageBox.Show("Введіть коректно дані посилання");
            }
            
            con.Close();
        }

        private void cmbModule_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetInfoFromDataBase();
            GetLinksFromModuleID();
        }

        private void dgvModuleLinks_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void GetInfoFromDataBase()
        {
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            Module mdl = (Module)cmbModule.SelectedItem;
            SqlDataAdapter DA = new SqlDataAdapter($"SELECT * FROM Link WHERE ModuleID = { mdl.id}", con);

            DataSet DS = new DataSet();
            DA.Fill(DS, "MD");

            dgvModuleLinks.DataSource = DS.Tables["MD"];
            dgvModuleLinks.Refresh();

            con.Close();
        }

        private void GetLinksFromModuleID()
        {
            Module mdl = (Module)cmbModule.SelectedItem;
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            SqlCommand comm = new SqlCommand($"select * from [dbo].[Link] L WHERE L.ModuleID = {mdl.id}", con);
            SqlDataReader DR = comm.ExecuteReader();

            cmbLinkID.Items.Clear();
            while (DR.Read())
            {
                Link lnk = new Link(int.Parse(DR[0].ToString()), DR[1].ToString());
                cmbLinkID.Items.Add(lnk);
                cmbLinkID.DisplayMember = "link";
            }

            con.Close();
        }

        private void btnMainMenu_Click(object sender, EventArgs e)
        {
            ActiveForm.Hide();
            MainMenu Menu = new MainMenu();
            Menu.ShowDialog();
            Close();
        }

        private void btnCreateNewModule_Click(object sender, EventArgs e)
        {
            ActiveForm.Hide();
            CreateNewModule CNM = new CreateNewModule();
            CNM.ShowDialog();
            Close();
        }

        private void btnRewriteLink_Click(object sender, EventArgs e)
        {
            Link lnk = (Link)cmbLinkID.SelectedItem;
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            try
            {
                if (txtLinkRewrite.Text == "" || txtLinkRewrite.Text.Length > 50 || lnk == null)
                {
                    throw new Exception();
                }
                SqlCommand comm = new SqlCommand($"UPDATE [dbo].[Link] SET Link = '{txtLinkRewrite.Text}' WHERE ID = {lnk.id}", con);
                comm.ExecuteNonQuery();
                GetInfoFromDataBase();
                GetLinksFromModuleID();
                con.Close();

                txtLinkRewrite.Text = null;
            }
            catch (Exception)
            {
                MessageBox.Show("Виберіть та відкорегуйте посилання з модулю");
            }

            con.Close();
        }

        private void cmbLinkID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Link lnk = (Link)cmbLinkID.SelectedItem;
            txtLinkRewrite.Text = lnk.link;
        }
    }
}
