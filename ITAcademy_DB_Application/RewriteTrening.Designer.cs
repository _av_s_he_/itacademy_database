﻿namespace ITAcademy_DB_Application
{
    partial class RewriteTrening
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnMainMenu = new System.Windows.Forms.Button();
            this.cmbTrening = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTreningName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtStartTrening = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtEndTrening = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbTrenerID = new System.Windows.Forms.ComboBox();
            this.cmbTrener = new System.Windows.Forms.ComboBox();
            this.btnRewriteTrening = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTrenerName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTrenerBD = new System.Windows.Forms.TextBox();
            this.btnRewriteTrener = new System.Windows.Forms.Button();
            this.cmbTreningQuestion = new System.Windows.Forms.ComboBox();
            this.cmbModuleQuestion = new System.Windows.Forms.ComboBox();
            this.cmbTestQuestion = new System.Windows.Forms.ComboBox();
            this.cmbQuestion = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtQuestion = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtAnswer1 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtAnswer2 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtAnswer3 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtRightAnswer = new System.Windows.Forms.TextBox();
            this.btnRewriteQuestion = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.cmbQuestionTestID = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnMainMenu
            // 
            this.btnMainMenu.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnMainMenu.ForeColor = System.Drawing.Color.Lime;
            this.btnMainMenu.Location = new System.Drawing.Point(13, 13);
            this.btnMainMenu.Name = "btnMainMenu";
            this.btnMainMenu.Size = new System.Drawing.Size(135, 62);
            this.btnMainMenu.TabIndex = 0;
            this.btnMainMenu.Text = "<-- Назад на головне меню";
            this.btnMainMenu.UseVisualStyleBackColor = false;
            this.btnMainMenu.Click += new System.EventHandler(this.btnMainMenu_Click);
            // 
            // cmbTrening
            // 
            this.cmbTrening.BackColor = System.Drawing.SystemColors.Desktop;
            this.cmbTrening.ForeColor = System.Drawing.Color.Lime;
            this.cmbTrening.FormattingEnabled = true;
            this.cmbTrening.Location = new System.Drawing.Point(27, 99);
            this.cmbTrening.Name = "cmbTrening";
            this.cmbTrening.Size = new System.Drawing.Size(351, 23);
            this.cmbTrening.TabIndex = 1;
            this.cmbTrening.Text = "Виберіть тренінг для редагування";
            this.cmbTrening.SelectedIndexChanged += new System.EventHandler(this.cmbTrening_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Lime;
            this.label1.Location = new System.Drawing.Point(27, 129);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(178, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Оновіть інформацію про назву";
            // 
            // txtTreningName
            // 
            this.txtTreningName.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtTreningName.ForeColor = System.Drawing.Color.Lime;
            this.txtTreningName.Location = new System.Drawing.Point(27, 148);
            this.txtTreningName.Name = "txtTreningName";
            this.txtTreningName.Size = new System.Drawing.Size(351, 23);
            this.txtTreningName.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Lime;
            this.label2.Location = new System.Drawing.Point(27, 178);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(218, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Оновіть інформацію про дату початку";
            // 
            // txtStartTrening
            // 
            this.txtStartTrening.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtStartTrening.ForeColor = System.Drawing.Color.Lime;
            this.txtStartTrening.Location = new System.Drawing.Point(27, 197);
            this.txtStartTrening.Name = "txtStartTrening";
            this.txtStartTrening.Size = new System.Drawing.Size(351, 23);
            this.txtStartTrening.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Lime;
            this.label3.Location = new System.Drawing.Point(27, 227);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(241, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Оновіть інформацію про дату завершення";
            // 
            // txtEndTrening
            // 
            this.txtEndTrening.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtEndTrening.ForeColor = System.Drawing.Color.Lime;
            this.txtEndTrening.Location = new System.Drawing.Point(27, 246);
            this.txtEndTrening.Name = "txtEndTrening";
            this.txtEndTrening.Size = new System.Drawing.Size(351, 23);
            this.txtEndTrening.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Lime;
            this.label4.Location = new System.Drawing.Point(27, 276);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(192, 15);
            this.label4.TabIndex = 8;
            this.label4.Text = "Оновіть інформацію про тренера";
            // 
            // cmbTrenerID
            // 
            this.cmbTrenerID.BackColor = System.Drawing.SystemColors.Desktop;
            this.cmbTrenerID.ForeColor = System.Drawing.Color.Lime;
            this.cmbTrenerID.FormattingEnabled = true;
            this.cmbTrenerID.Location = new System.Drawing.Point(27, 295);
            this.cmbTrenerID.Name = "cmbTrenerID";
            this.cmbTrenerID.Size = new System.Drawing.Size(351, 23);
            this.cmbTrenerID.TabIndex = 9;
            // 
            // cmbTrener
            // 
            this.cmbTrener.BackColor = System.Drawing.SystemColors.Desktop;
            this.cmbTrener.ForeColor = System.Drawing.Color.Lime;
            this.cmbTrener.FormattingEnabled = true;
            this.cmbTrener.Location = new System.Drawing.Point(971, 99);
            this.cmbTrener.Name = "cmbTrener";
            this.cmbTrener.Size = new System.Drawing.Size(310, 23);
            this.cmbTrener.TabIndex = 10;
            this.cmbTrener.Text = "Виберіть тренера для оновлення інформації";
            this.cmbTrener.SelectedIndexChanged += new System.EventHandler(this.cmbTrener_SelectedIndexChanged);
            // 
            // btnRewriteTrening
            // 
            this.btnRewriteTrening.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnRewriteTrening.ForeColor = System.Drawing.Color.Lime;
            this.btnRewriteTrening.Location = new System.Drawing.Point(429, 250);
            this.btnRewriteTrening.Name = "btnRewriteTrening";
            this.btnRewriteTrening.Size = new System.Drawing.Size(158, 68);
            this.btnRewriteTrening.TabIndex = 11;
            this.btnRewriteTrening.Text = "Оновити інформацію про тренінг";
            this.btnRewriteTrening.UseVisualStyleBackColor = false;
            this.btnRewriteTrening.Click += new System.EventHandler(this.btnRewriteTrening_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Lime;
            this.label5.Location = new System.Drawing.Point(971, 129);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(169, 15);
            this.label5.TabIndex = 12;
            this.label5.Text = "Оновіть інформацію про ім\'я";
            // 
            // txtTrenerName
            // 
            this.txtTrenerName.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtTrenerName.ForeColor = System.Drawing.Color.Lime;
            this.txtTrenerName.Location = new System.Drawing.Point(971, 147);
            this.txtTrenerName.Name = "txtTrenerName";
            this.txtTrenerName.Size = new System.Drawing.Size(310, 23);
            this.txtTrenerName.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Lime;
            this.label6.Location = new System.Drawing.Point(971, 178);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(242, 15);
            this.label6.TabIndex = 14;
            this.label6.Text = "Оновіть інформацію про дату народження";
            // 
            // txtTrenerBD
            // 
            this.txtTrenerBD.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtTrenerBD.ForeColor = System.Drawing.Color.Lime;
            this.txtTrenerBD.Location = new System.Drawing.Point(971, 196);
            this.txtTrenerBD.Name = "txtTrenerBD";
            this.txtTrenerBD.Size = new System.Drawing.Size(310, 23);
            this.txtTrenerBD.TabIndex = 15;
            // 
            // btnRewriteTrener
            // 
            this.btnRewriteTrener.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnRewriteTrener.ForeColor = System.Drawing.Color.Lime;
            this.btnRewriteTrener.Location = new System.Drawing.Point(1055, 250);
            this.btnRewriteTrener.Name = "btnRewriteTrener";
            this.btnRewriteTrener.Size = new System.Drawing.Size(158, 68);
            this.btnRewriteTrener.TabIndex = 16;
            this.btnRewriteTrener.Text = "Оновити інформацію про тренера";
            this.btnRewriteTrener.UseVisualStyleBackColor = false;
            this.btnRewriteTrener.Click += new System.EventHandler(this.btnRewriteTrener_Click);
            // 
            // cmbTreningQuestion
            // 
            this.cmbTreningQuestion.BackColor = System.Drawing.SystemColors.Desktop;
            this.cmbTreningQuestion.ForeColor = System.Drawing.Color.Lime;
            this.cmbTreningQuestion.FormattingEnabled = true;
            this.cmbTreningQuestion.Location = new System.Drawing.Point(27, 395);
            this.cmbTreningQuestion.Name = "cmbTreningQuestion";
            this.cmbTreningQuestion.Size = new System.Drawing.Size(351, 23);
            this.cmbTreningQuestion.TabIndex = 17;
            this.cmbTreningQuestion.Text = "Виберіть тренінг для редагування питання";
            this.cmbTreningQuestion.SelectedIndexChanged += new System.EventHandler(this.cmbTreningQuestion_SelectedIndexChanged);
            // 
            // cmbModuleQuestion
            // 
            this.cmbModuleQuestion.BackColor = System.Drawing.SystemColors.Desktop;
            this.cmbModuleQuestion.ForeColor = System.Drawing.Color.Lime;
            this.cmbModuleQuestion.FormattingEnabled = true;
            this.cmbModuleQuestion.Location = new System.Drawing.Point(27, 425);
            this.cmbModuleQuestion.Name = "cmbModuleQuestion";
            this.cmbModuleQuestion.Size = new System.Drawing.Size(351, 23);
            this.cmbModuleQuestion.TabIndex = 18;
            this.cmbModuleQuestion.Text = "Виберіть модуль для редагування питання";
            this.cmbModuleQuestion.SelectedIndexChanged += new System.EventHandler(this.cmbModuleQuestion_SelectedIndexChanged);
            // 
            // cmbTestQuestion
            // 
            this.cmbTestQuestion.BackColor = System.Drawing.SystemColors.Desktop;
            this.cmbTestQuestion.ForeColor = System.Drawing.Color.Lime;
            this.cmbTestQuestion.FormattingEnabled = true;
            this.cmbTestQuestion.Location = new System.Drawing.Point(27, 455);
            this.cmbTestQuestion.Name = "cmbTestQuestion";
            this.cmbTestQuestion.Size = new System.Drawing.Size(351, 23);
            this.cmbTestQuestion.TabIndex = 19;
            this.cmbTestQuestion.Text = "Виберіть тест для редагування питання";
            this.cmbTestQuestion.SelectedIndexChanged += new System.EventHandler(this.cmbTestQuestion_SelectedIndexChanged);
            // 
            // cmbQuestion
            // 
            this.cmbQuestion.BackColor = System.Drawing.SystemColors.Desktop;
            this.cmbQuestion.ForeColor = System.Drawing.Color.Lime;
            this.cmbQuestion.FormattingEnabled = true;
            this.cmbQuestion.Location = new System.Drawing.Point(27, 485);
            this.cmbQuestion.Name = "cmbQuestion";
            this.cmbQuestion.Size = new System.Drawing.Size(351, 23);
            this.cmbQuestion.TabIndex = 20;
            this.cmbQuestion.Text = "Виберіть питання для редагування";
            this.cmbQuestion.SelectedIndexChanged += new System.EventHandler(this.cmbQuestion_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Lime;
            this.label7.Location = new System.Drawing.Point(27, 525);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(193, 15);
            this.label7.TabIndex = 21;
            this.label7.Text = "Оновіть інформацію про питання";
            // 
            // txtQuestion
            // 
            this.txtQuestion.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtQuestion.ForeColor = System.Drawing.Color.Lime;
            this.txtQuestion.Location = new System.Drawing.Point(27, 544);
            this.txtQuestion.Name = "txtQuestion";
            this.txtQuestion.Size = new System.Drawing.Size(351, 23);
            this.txtQuestion.TabIndex = 22;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Lime;
            this.label8.Location = new System.Drawing.Point(448, 525);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(260, 15);
            this.label8.TabIndex = 23;
            this.label8.Text = "Оновіть інформацію про варіант відповіді №1";
            // 
            // txtAnswer1
            // 
            this.txtAnswer1.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtAnswer1.ForeColor = System.Drawing.Color.Lime;
            this.txtAnswer1.Location = new System.Drawing.Point(448, 543);
            this.txtAnswer1.Name = "txtAnswer1";
            this.txtAnswer1.Size = new System.Drawing.Size(319, 23);
            this.txtAnswer1.TabIndex = 24;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.Lime;
            this.label9.Location = new System.Drawing.Point(846, 525);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(260, 15);
            this.label9.TabIndex = 25;
            this.label9.Text = "Оновіть інформацію про варіант відповіді №2";
            // 
            // txtAnswer2
            // 
            this.txtAnswer2.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtAnswer2.ForeColor = System.Drawing.Color.Lime;
            this.txtAnswer2.Location = new System.Drawing.Point(846, 544);
            this.txtAnswer2.Name = "txtAnswer2";
            this.txtAnswer2.Size = new System.Drawing.Size(319, 23);
            this.txtAnswer2.TabIndex = 26;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Lime;
            this.label10.Location = new System.Drawing.Point(27, 608);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(260, 15);
            this.label10.TabIndex = 27;
            this.label10.Text = "Оновіть інформацію про варіант відповіді №3";
            // 
            // txtAnswer3
            // 
            this.txtAnswer3.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtAnswer3.ForeColor = System.Drawing.Color.Lime;
            this.txtAnswer3.Location = new System.Drawing.Point(27, 627);
            this.txtAnswer3.Name = "txtAnswer3";
            this.txtAnswer3.Size = new System.Drawing.Size(351, 23);
            this.txtAnswer3.TabIndex = 28;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.Lime;
            this.label11.Location = new System.Drawing.Point(448, 608);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(308, 15);
            this.label11.TabIndex = 29;
            this.label11.Text = "Оновіть інформацію про правильний варіант відповіді";
            // 
            // txtRightAnswer
            // 
            this.txtRightAnswer.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtRightAnswer.ForeColor = System.Drawing.Color.Lime;
            this.txtRightAnswer.Location = new System.Drawing.Point(448, 627);
            this.txtRightAnswer.Name = "txtRightAnswer";
            this.txtRightAnswer.Size = new System.Drawing.Size(319, 23);
            this.txtRightAnswer.TabIndex = 30;
            // 
            // btnRewriteQuestion
            // 
            this.btnRewriteQuestion.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnRewriteQuestion.ForeColor = System.Drawing.Color.Lime;
            this.btnRewriteQuestion.Location = new System.Drawing.Point(448, 423);
            this.btnRewriteQuestion.Name = "btnRewriteQuestion";
            this.btnRewriteQuestion.Size = new System.Drawing.Size(158, 68);
            this.btnRewriteQuestion.TabIndex = 31;
            this.btnRewriteQuestion.Text = "Оновити інформацію про питання";
            this.btnRewriteQuestion.UseVisualStyleBackColor = false;
            this.btnRewriteQuestion.Click += new System.EventHandler(this.btnRewriteQuestion_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.Lime;
            this.label12.Location = new System.Drawing.Point(846, 608);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(324, 15);
            this.label12.TabIndex = 32;
            this.label12.Text = "Оновіть інформацію про тест до якого належить питання";
            // 
            // cmbQuestionTestID
            // 
            this.cmbQuestionTestID.BackColor = System.Drawing.SystemColors.Desktop;
            this.cmbQuestionTestID.ForeColor = System.Drawing.Color.Lime;
            this.cmbQuestionTestID.FormattingEnabled = true;
            this.cmbQuestionTestID.Location = new System.Drawing.Point(846, 627);
            this.cmbQuestionTestID.Name = "cmbQuestionTestID";
            this.cmbQuestionTestID.Size = new System.Drawing.Size(319, 23);
            this.cmbQuestionTestID.TabIndex = 33;
            this.cmbQuestionTestID.Text = "Виберіть питання для початку";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label13.ForeColor = System.Drawing.Color.Lime;
            this.label13.Location = new System.Drawing.Point(190, 13);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(413, 17);
            this.label13.TabIndex = 34;
            this.label13.Text = "Для редагування тренінгу виберіть його з випадаючого списку";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label14.ForeColor = System.Drawing.Color.Lime;
            this.label14.Location = new System.Drawing.Point(190, 34);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(442, 17);
            this.label14.TabIndex = 35;
            this.label14.Text = "Для редагування питань для тесту всі відповідні випадаючі списки";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label15.ForeColor = System.Drawing.Color.Lime;
            this.label15.Location = new System.Drawing.Point(190, 55);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(491, 17);
            this.label15.TabIndex = 36;
            this.label15.Text = " Для редагування інформацію про тренера оберіть його зі списку тренерів";
            // 
            // RewriteTrening
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Desktop;
            this.ClientSize = new System.Drawing.Size(1293, 707);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.cmbQuestionTestID);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.btnRewriteQuestion);
            this.Controls.Add(this.txtRightAnswer);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtAnswer3);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtAnswer2);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtAnswer1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtQuestion);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cmbQuestion);
            this.Controls.Add(this.cmbTestQuestion);
            this.Controls.Add(this.cmbModuleQuestion);
            this.Controls.Add(this.cmbTreningQuestion);
            this.Controls.Add(this.btnRewriteTrener);
            this.Controls.Add(this.txtTrenerBD);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtTrenerName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnRewriteTrening);
            this.Controls.Add(this.cmbTrener);
            this.Controls.Add(this.cmbTrenerID);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtEndTrening);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtStartTrening);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtTreningName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbTrening);
            this.Controls.Add(this.btnMainMenu);
            this.Name = "RewriteTrening";
            this.Text = "RewriteTrening";
            this.Load += new System.EventHandler(this.RewriteTrening_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnMainMenu;
        private System.Windows.Forms.ComboBox cmbTrening;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTreningName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtStartTrening;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtEndTrening;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbTrenerID;
        private System.Windows.Forms.ComboBox cmbTrener;
        private System.Windows.Forms.Button btnRewriteTrening;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtTrenerName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtTrenerBD;
        private System.Windows.Forms.Button btnRewriteTrener;
        private System.Windows.Forms.ComboBox cmbTreningQuestion;
        private System.Windows.Forms.ComboBox cmbModuleQuestion;
        private System.Windows.Forms.ComboBox cmbTestQuestion;
        private System.Windows.Forms.ComboBox cmbQuestion;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtQuestion;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtAnswer1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtAnswer2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtAnswer3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtRightAnswer;
        private System.Windows.Forms.Button btnRewriteQuestion;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cmbQuestionTestID;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
    }
}