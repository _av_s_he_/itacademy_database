﻿namespace ITAcademy_DB_Application
{
    partial class INSERT
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCreateNewTrening = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btnSetModuleLinks = new System.Windows.Forms.Button();
            this.btnMainMenu = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCreateNewTrening
            // 
            this.btnCreateNewTrening.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnCreateNewTrening.Location = new System.Drawing.Point(662, 350);
            this.btnCreateNewTrening.Name = "btnCreateNewTrening";
            this.btnCreateNewTrening.Size = new System.Drawing.Size(147, 56);
            this.btnCreateNewTrening.TabIndex = 3;
            this.btnCreateNewTrening.Text = "Створити новий тренінг тренером -->";
            this.btnCreateNewTrening.UseVisualStyleBackColor = false;
            this.btnCreateNewTrening.Click += new System.EventHandler(this.btnCreateNewTrening_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.Desktop;
            this.button2.Location = new System.Drawing.Point(474, 350);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(148, 56);
            this.button2.TabIndex = 2;
            this.button2.Text = "   Створити новий модуль -->";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.Desktop;
            this.button3.Location = new System.Drawing.Point(660, 230);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(149, 57);
            this.button3.TabIndex = 1;
            this.button3.Text = "Записати користувача на тренінг -->";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnSetModuleLinks
            // 
            this.btnSetModuleLinks.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnSetModuleLinks.Location = new System.Drawing.Point(474, 230);
            this.btnSetModuleLinks.Name = "btnSetModuleLinks";
            this.btnSetModuleLinks.Size = new System.Drawing.Size(149, 57);
            this.btnSetModuleLinks.TabIndex = 0;
            this.btnSetModuleLinks.Text = "Заповнити посилання на модуль -->";
            this.btnSetModuleLinks.UseVisualStyleBackColor = false;
            this.btnSetModuleLinks.Click += new System.EventHandler(this.FirstTableFilling_Click);
            // 
            // btnMainMenu
            // 
            this.btnMainMenu.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnMainMenu.Location = new System.Drawing.Point(12, 12);
            this.btnMainMenu.Name = "btnMainMenu";
            this.btnMainMenu.Size = new System.Drawing.Size(132, 54);
            this.btnMainMenu.TabIndex = 4;
            this.btnMainMenu.Text = "<-- Назад на головне меню";
            this.btnMainMenu.UseVisualStyleBackColor = false;
            this.btnMainMenu.Click += new System.EventHandler(this.btnMainMenu_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Desktop;
            this.ClientSize = new System.Drawing.Size(1319, 720);
            this.Controls.Add(this.btnMainMenu);
            this.Controls.Add(this.btnSetModuleLinks);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnCreateNewTrening);
            this.ForeColor = System.Drawing.Color.Lime;
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCreateNewTrening;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnSetModuleLinks;
        private System.Windows.Forms.Button btnMainMenu;
    }
}